/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>
#include <iostream>
#include <cstring>  

using namespace std;
int CalcLength(string s);

int main()
{
    string s = "";
    cin>>s;

    cout<<CalcLength(s)<<endl;
    return 0;
}

int CalcLength(string s) {
    int i = -1;
    while (s[++i] != '\0');
    return i;
}
