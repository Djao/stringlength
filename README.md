# Question 9

## Description

<details><summary>Click to expand</summary>
Make a program in C/C++ or Java language that calculates the length of a string informed by the user (do not use any pre-existing function or method for this, such as len(), count(), strlen() or lenght());
</details>

## Live-Test

In this link [https://onlinegdb.com/SDqYvZ9vX](https://onlinegdb.com/SDqYvZ9vX) it's possible to run the solution online.

## Comments

The function counts how many characters are present in a string before it finds the null '\0' character that represents end of string.
